const express = require("express");
const todoController = require("../controllers/todo.controller");
const router = express.Router();

/**
 * 
 * @api {POST} /todo/ Add new ToDo
 * @apiName ToDo
 * @apiGroup /todo/
 * @apiVersion 0.0.1
 * 
 * @apiParam  {String} title title of the new ToDo
 * @apiParam  {Boolean} done state of the ToDo
 * 
 * @apiSuccess (200) {String} title title of the new ToDo
 * @apiSuccess (200) {Boolean} done state of the new ToDo
 * @apiSuccess (200) {String} _id id of the inserted ToDo
 * 
 * @apiParamExample  {json} Request-Example:
 * {
 *      "title": "Make a manual test",
 *      "done": false
 * }
 * 
 * 
 * @apiSuccessExample {json} Success-Response:
 * {
 *      "title": "Make a manual test",
 *      "done": false,
 *      "_id": 213
 * }
 * 
 * @apiError (404) ToDoNotCreated The ToDo could not be created.
 * 
 * @apiErrorExample {json} Error-Response:
 * HTTP/1.1 404 Not Found
 * {
 *      "error": "UserNotFound"
 * }
 * 
 * 
 */
router.post("/", todoController.createTodo);

module.exports = router;