const express = require("express");
const todoRoutes = require("./routes/todo.routes");
const app = express();
const mongodb = require("./mongodb/mongodb.connect");

mongodb.connect("mongodb+srv://SuperTestUser:SuperTestUser1@sandbox.wjuyg.mongodb.net/todo-tdd?retryWrites=true&w=majority");

app.use(express.json());

app.use("/todos", todoRoutes);

/**
 * 
 * @api {GET} / Hello World
 * @apiName ToDo
 * @apiGroup /
 * @apiVersion 0.0.1
 * 
 * 
 * @apiSuccess (200) {String} message server message
 * 
 * @apiSuccessExample {json} Success-Response:
 * {
 *     "message" : "Hello World!"
 * }
 * 
 * 
 */
app.get("/", (req, res) => {
    res.json({ "message": "Hello world!"});
});

module.exports = app;