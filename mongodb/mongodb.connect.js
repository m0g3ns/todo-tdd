const mongoose = require("mongoose");

async function connect(url) {
    try {
        await mongoose.connect(url || "mongodb+srv://SuperTestUser:SuperTestUser1@sandbox.wjuyg.mongodb.net/todo-tdd?retryWrites=true&w=majority");
    } catch(err) {
        console.error("Error connecting to mongodb");
        console.error(err);
    }
}

module.exports = { connect };
