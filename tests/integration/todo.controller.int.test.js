const request = require("supertest");
const app = require("../../app");
const newTodo = require("../mock-data/new-todo.json");
const helloWorld = require("../mock-data/hello-world.json");
const endpointUrl = "/todos/";

describe(endpointUrl, () => {
    it("POST /todos/", async () => { 
        const response = await request(app)
            .post(endpointUrl)
            .send(newTodo);
        expect(response.statusCode).toBe(201);
        expect(response.body.title).toBe(newTodo.title);
        expect(response.body.done).toBe(newTodo.done);
    })
});

describe("GET /", () => {
    it("GET /", async () => {
        const response = await request(app).get("/");        
        expect(response.body).toStrictEqual(helloWorld);
    });
});